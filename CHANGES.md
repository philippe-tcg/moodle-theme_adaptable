Changes in version 1.1.3 (2016022601)
-------------------------------------

- Fix #60 Add support for Social Wall courese format
- Fix #363 Add padding setting for main navigation items
- Fix #362 Add caching for main navigation bar
- Fix #355 Add link to font awesome icon list in social settings
- Fix #315 Resolve bugs on theme select

Changes in version 1.1.2 (2016022502)
-------------------------------------

- Fix #345 One full width marketing block not displayed
- Fix #360 Remove es language files
- Fix #361 Make My Course list settings more generic
- Fix #353 Add feature for custom sorting of My Courses
- Fix #349 End slash in the social icon url error
- Fix #348 Remove default facebook social icon
- Fix #353 Add feature for custom sorting of My Courses
- Fix #352 Move hidden courses into sub menu
- Fix #273 PHP errors after install
- Fix #334 Include Font Awesome in the theme
- Fix #336 Remove Global Font Size from settings
- Fix #268 Quiz settings spill out of the main region
- Fix #247 Mail settings spill on blocks
- Fix #331 Remove lecacy contact details style
- Fix #327 Fix show / hide front page marketing blocks

Changes in version 1.1.1 (2016020400)
-------------------------------------

NOTE: After upgrading go to the "Header Navbar" settings and save to update the "enablemysites" field with new values

- Fix #304 Improve css and add setting to control length of text displayed in my courses dropdown
- Fix #326 Title using double line overlap text
- Fix #313 Delete Block Button Color Incorrect
- Fix #317 Wrong ul in flexslider
- Fix #280 Partial fix removing some title attributes from navigation and improving alignment of navbar dropdown menus
- Fix #249 Increaes messages dropdown size to avoid spill, corrected issue with left align and improved spacing
- Fix #320 Remove notifications from messages, this needs to be put back as an option in admin settings
- Fix #318 Catch notices from renderer caused by undefined variables
- Fix #316 Increase font size range for Top Menu & Navigation bar
- Fix #184 Topic title header width changes when editing On
- Fix #285 Slideshow is not working in RTL sites
- Fix #311 Undefined variable: /theme/adaptable/renderers.php on line 747
- Fix #277 There are 2 marketing blocks settings separated from the sub-section
- Fix #304 Reduce length of course titles in my course dropdown list to avoid spill
- Fix #309 Add option to show hidden courses with icons in my course dropdown list
- Fix #308 Fix styling issue in gapselect question type
- Fix #305 Correct question text layout in lesson module
- Fix #306 Correct links to readme file in admin pages
- Fix #268 Fix quiz settings spilling out of container
- Fix #307 Fix feature to use course shortcode in header
- Fix #298 Show larger / sharper profile image in forum posts
- Fix #301 Show larger profile image when clicking to view profile in forum
- Fix #299 Correct default settings for marketing and footer layout builders

Changes in version 1.1.0 (2016012700)
-------------------------------------

- Fix #296 - if not already set define looped settings with a default value
- Fix #295 adding color pickers to sliders
- Fix #291 add renderer, finish layout builder in settings, fix incorrect lang strings, make show / hide use PHP to ...
- Fix #291 Rewrite footer settings adding layout builder
- Fix #284 adding block region builder
